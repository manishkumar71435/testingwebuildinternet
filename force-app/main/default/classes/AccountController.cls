//Adding Comment
public with sharing class accountController {
    @AuraEnabled(cacheable=true)
    public static List<Account> GetAccounts() {
        return [
            SELECT Name, AnnualRevenue, Industry
            FROM Account
            WITH SECURITY_ENFORCED
            ORDER BY Name
        ];
    }
}
