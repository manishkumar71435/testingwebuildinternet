public class ObjectPicklistController1 {

    public String selectedObject {get;set;}
    public List<sObject> objFields {get;set;} 
    public List<String> result {get;set;}
    
    public ObjectPicklistController1(){ 
        selectedObject = 'none';
    }
    
    public List<SelectOption> getobjectNames(){
        List<Schema.SObjectType> gd = Schema.getGlobalDescribe().Values();    
        List<SelectOption> options = new List<SelectOption>();
        for(Schema.SObjectType f : gd){
            options.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getName()));
            options.sort();        
        }
        return options;
    }
  
    public void fetchFields(){ 
        result = new List<String>();
        Map<String , Schema.SObjectType> globalDescription = Schema.getGlobalDescribe();
        Schema.sObjectType objType = globalDescription.get(selectedObject);   
        Schema.DescribeSObjectResult r1 = objType.getDescribe(); 
        
        Map<String , Schema.SObjectField> mapFieldList = r1.fields.getMap();  

        for(Schema.SObjectField field : mapFieldList.values()){  
            Schema.DescribeFieldResult fieldResult = field.getDescribe();  
            
            if(fieldResult.isAccessible()){  
                result.add(fieldResult.getName());
            }  
        }
        Integer i = 0;
        String fieldsToFetch = '';
        for(String temp:result){       
            Integer len = result.size();
            if(i==len-1){
                  fieldsToFetch = fieldsToFetch + temp;
            }
            else{
                  fieldsToFetch = fieldsToFetch + temp + ',';
            }
            i++;
        }
        try{
            String sql = ' SELECT ' + fieldsToFetch + ' FROM ' + selectedObject + ' ORDER BY CreatedDate DESC LIMIT 5';
            objFields = Database.Query(sql);
        }
        catch(Exception e){
            ApexPages.addMessages(e);
        }
    }   
}