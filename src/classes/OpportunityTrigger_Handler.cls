/*
    Purpose: Adding Process Builder Functionality to Apex
    PB - "Update Preferred Reseller on Opportunity", "Update Bill To Account on Opportunity", "Opportunity"
*/
public class OpportunityTrigger_Handler {

    //Process Builder Name - "Update Preferred Reseller on Opportunity"
    public static void updatePreferredResellerOnOpportunity(List<Opportunity> newOpportunityList){
        Set<Id> relatedAccIds = new Set<Id>();
        Set<Id> relatedConIds = new Set<Id>();
        Map<Id,Account> accMap;
        Map<Id,Contact> conMap;
        
        System.debug('--------'+newOpportunityList);
        for(Opportunity opp: newOpportunityList){
            if(opp.Preferred_Account__c != null){
                relatedAccIds.add(opp.Preferred_Account__c);
            }
            if(opp.Renewed_Contact__c!= null){
                relatedConIds.add(opp.Renewed_Contact__c);
                System.debug('---relatedConIds----->>'+relatedConIds);
            }
        }
        
        if(!relatedAccIds.isEmpty()){
            accMap = new Map<Id,Account>([SELECT Id, Name FROM Account WHERE ID IN : relatedAccIds ]);
        }
        
        if(!relatedConIds.isEmpty()){
            conMap= new Map<Id,Contact>([SELECT Id, Name,Related_Opportunity__r.Preferred_Account__c, AccountId FROM Contact WHERE ID IN : relatedConIds ]);
            System.debug('---conMap----->>'+conMap);
        }
        
        for(Opportunity opp1: newOpportunityList){
            Opportunity opp;
            Account acc;
            Contact con;
            
            if(opp == null){
                if(opp1.NextStep != null && conMap != null && conMap.containsKey(opp1.Renewed_Contact__c)){
                    con= conMap.get(opp1.Renewed_Contact__c);
                }
            }
            System.debug('---FOR----->>'+con);
            if(con!= null){
                System.debug('---FOR-----<<----'+con.Related_Opportunity__r.Preferred_Account__c);
                opp1.AccountId =  con.Related_Opportunity__r.Preferred_Account__c;
            }/*
            System.debug('---FOR-----'+opp);
            //Check for Deal Registration Update
            if(opp.AccountId == null){
                System.debug('--------'+opp.Preferred_Account__r.Id);
                System.debug('--------'+opp.Renewed_Contact__r.Related_Opportunity__r.Preferred_Account__c);
                System.debug('--------'+opp.Renewed_Contact__r.Related_Opportunity__r.Name);
                if(opp.NextStep != null && opp.Preferred_Account__r.Id != null){
                    opp.AccountId = opp.Preferred_Account__r.Id;
                }
                
                //Check for Renewal Opportunity
                else if(opp.Renew_Contact__c == TRUE){
                    opp.AccountId =  opp.Renewed_Contact__r.Related_Opportunity__r.Preferred_Account__c;
                }
                
                //Check for Synced Quote
                else if(opp.AccountId == null && opp.Renewed_Contact__r.Related_Opportunity__r.Preferred_Account__c != null){
                    opp.Name=  'ELSE IF> '+opp.Renewed_Contact__r.Related_Opportunity__r.Name;
                }
            }*/
        }
    }
    /*
    //Process Builder Name - "Update Bill To Account on Opportunity"
    public static void updateBillToAccountOnOpportunity(List<Opportunity> newOpportunityList, Map<Id, Opportunity> oldOpportunityMap){
        for(Opportunity opp: newOpportunityList){
        
            //Distributor & Reseller are Blank
            if(opp.BillTo__c == null || opp.AccountId != oldOpportunityMap.get(opp.Id).AccountId || opp.Sold_To_Partner__c != null || opp.Tier_2_Partner__c != null){
                if(opp.Sold_To_Partner__c == null && opp.Tier_2_Partner__c == null){
                    opp.BillTo__c = opp.AccountId;
                }
                
                //Distributor is Blank & Reseller NOT Blank
                else if(opp.Sold_To_Partner__c == null && opp.Tier_2_Partner__c != null){
                    opp.BillTo__c = opp.Tier_2_Partner__r.Id;
                }
                
                //Distributor is NOT Blank
                else if(opp.Sold_To_Partner__c != null){
                    opp.BillTo__c = opp.Sold_To_Partner__r.Id;
                }
            }
            
            //Distributor & Reseller Removed
            else if((opp.Sold_To_Partner__c != oldOpportunityMap.get(opp.Id).Sold_To_Partner__c || opp.Tier_2_Partner__c != oldOpportunityMap.get(opp.Id).Tier_2_Partner__c) && opp.Sold_To_Partner__c == null && opp.Tier_2_Partner__c == null){
                opp.BillTo__c = opp.AccountId;
            }
        }
    }
    
    //Process Builder Name - "Opportunity"
    public static void updateOpportunity(List<Opportunity> newOpportunityList, Map<Id, Opportunity> oldOpportunityMap){
        for(Opportunity opp: newOpportunityList){
        
            //Renewal Opportunity
            if(opp.SBQQ__Renewal__c == True && opp.SBQQ__RenewedContract__c != null && opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__c != null && (oldOpportunityMap == null || oldOpportunityMap.isEmpty())){
                opp.CloseDate = opp.SBQQ__RenewedContract__r.EndDate;
                opp.LeadSource = 'Sales Generated';
                opp.Name = 'Renewal'+ opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.Name;
                opp.OwnerId = opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.OwnerId;
                opp.Preferred_Reseller__c = opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.Preferred_Reseller__c;
                opp.Primary_Contact__c = opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.Primary_Contact__c;
                opp.RNL_Type__c = 'Full';
                opp.Sold_To_Partner_Contact__c = opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.Sold_To_Partner_Contact__c;
                opp.Sold_To_Partner__c = opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.Sold_To_Partner__c;
                opp.Source_of_Business__c = 'Sales';
                opp.Support_Provider__c = opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.Support_Provider__c;
                opp.Tier_2_Partner_Contact__c = opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.Tier_2_Partner_Contact__c;
                opp.Tier_2_Partner__c = opp.SBQQ__RenewedContract__r.SBQQ__Opportunity__r.Tier_2_Partner__c;
                opp.Type = 'Renewal';
            }
            
            //Amendment Opportunity
            if(opp.SBQQ__AmendedContract__c != null && opp.SBQQ__AmendedContract__r.SBQQ__Opportunity__c != null && (oldOpportunityMap == null || oldOpportunityMap.isEmpty())){
                opp.Preferred_Reseller__c = opp.SBQQ__AmendedContract__r.SBQQ__Opportunity__r.Preferred_Reseller__c;
                opp.Primary_Contact__c = opp.SBQQ__AmendedContract__r.SBQQ__Opportunity__r.Primary_Contact__c;
                opp.Sold_To_Partner_Contact__c = opp.SBQQ__AmendedContract__r.SBQQ__Opportunity__r.Sold_To_Partner_Contact__c;
                opp.Sold_To_Partner__c = opp.SBQQ__AmendedContract__r.SBQQ__Opportunity__r.Sold_To_Partner__c;
                opp.Support_Provider__c = opp.SBQQ__AmendedContract__r.SBQQ__Opportunity__r.Support_Provider__c;
                opp.Tier_2_Partner_Contact__c = opp.SBQQ__AmendedContract__r.SBQQ__Opportunity__r.Tier_2_Partner_Contact__c;
                opp.Tier_2_Partner__c = opp.SBQQ__AmendedContract__r.SBQQ__Opportunity__r.Tier_2_Partner__c;
                opp.Type = 'Existing Business';
            }
            
            //Renewal Quoted
            if(opp.Renewal_Quoted__c == True && opp.SBQQ__RenewedContract__c != null){
                opp.Renewal_Quoted__c = False;
                //opp.Renewal_Quoted__c = True;
            }
            
            //StageChange
            else if(!oldOpportunityMap.isEmpty() || oldOpportunityMap != null){
                if(opp.Stage_2_Date_Time__c == null && (oldOpportunityMap.get(opp.Id).StageName == null || oldOpportunityMap.get(opp.Id).StageName == '1 - Qualify' || oldOpportunityMap.get(opp.Id).StageName == 'Lost') && opp.StageName != 'Lost' && opp.StageName != '1 - Qualify' && opp.StageName != oldOpportunityMap.get(opp.Id).StageName){
                    opp.Stage_2_Date_Time__c = System.now();
                }
            }
            
            //StageNew
            else if(opp.StageName != 'Lost' && opp.StageName != '1 - Qualify' && (oldOpportunityMap == null || oldOpportunityMap.isEmpty())){
                opp.Stage_2_Date_Time__c = System.now();
            }
        }
    }*/
}