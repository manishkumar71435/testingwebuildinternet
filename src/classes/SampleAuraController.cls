public class SampleAuraController {
    public string devoteeName{get;set;}
    Public List<string> leftselected{get;set;}
    Public List<string> rightselected{get;set;}
    Set<string> leftvalues = new Set<string>();
    Set<string> rightvalues = new Set<string>();
    
    public SampleAuraController() {
        System.debug('Constructor');
    }
    public PageReference selectclick(){
         System.debug('Mail Sent');
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
    }
     
    public PageReference unselectclick(){
         System.debug('Mail Sent');
        leftselected.clear();
        for(String s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
 
    public List<SelectOption> getunSelectedValues(){
         System.debug('Mail Sent');
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(string s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }
 
    public List<SelectOption> getSelectedValues(){
         System.debug('Mail Sent');
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(rightvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }
    
    public static void sendEmail() {
        System.debug('Mail Sent');
      List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
      Messaging.SingleEmailMessage mail = 
      new Messaging.SingleEmailMessage();
    
      List<String> sendTo = new List<String>();
      sendTo.add('khiladimanu1@gmail.com');
      mail.setToAddresses(sendTo);
    
      mail.setReplyTo('sirdavid@bankofnigeria.com');
      mail.setSenderDisplayName('Official Bank of Nigeria');
    
      List<String> ccTo = new List<String>();
      ccTo.add('business@bankofnigeria.com');
      mail.setCcAddresses(ccTo);

      mail.setSubject('URGENT BUSINESS PROPOSAL');
      String body = 'devoteeName';
      mail.setHtmlBody(body);
      Messaging.sendEmail(mails);
    }
}