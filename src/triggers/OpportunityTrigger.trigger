trigger OpportunityTrigger on Opportunity (before insert, before update, before delete, after insert, after update, after delete) {
    if(trigger.isBefore){
        System.debug('----iSBEFORE----');
        if(trigger.isInsert){
            System.debug('----iSBEFOREInsert----');
            OpportunityTrigger_Handler.updatePreferredResellerOnOpportunity(Trigger.new);
        }/*
        else if(trigger.isUpdate){
            
        }
        else if(trigger.isDelete){
            
        }*/
    }
    else if(trigger.isAfter){/*
        if(trigger.isInsert){
            
        }
        else if(trigger.isUpdate){
            
        }
        else if(trigger.isDelete){
            
        }*/
    }
}